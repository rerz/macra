use proc_macro::TokenStream;
use syn::{ItemFn, AttributeArgs};
use quote::ToTokens;

#[proc_macro_attribute]
pub fn macra(args: TokenStream, input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as ItemFn);
    let args = syn::parse_macro_input!(args as AttributeArgs);

    for meta in args {
        match meta {
            syn::NestedMeta::Lit(lit) => {
                dbg!(lit.to_token_stream());
            },
            syn::NestedMeta::Meta(meta) => {}
        }
    }

    let input_ident = &input.sig.ident;
    let mut original_fn = input.clone();

    original_fn.sig.ident = syn::Ident::new(
             &format!("__original_{}", input_ident),
            proc_macro2::Span::call_site(),
    );

    let original_ident = &original_fn.sig.ident;

    let target_syn_ty = quote::quote!(syn::ItemStruct);

    // // TODO: make this nicer
    // let target_syn_ty = match macrify_args.target.as_ref() {
    //     "struct" => quote!(syn::ItemStruct),
    //     "mod" => quote!(syn::ItemMod),
    //     "impl" => quote!(syn::ItemImpl),
    //     _ => panic!("unsupported target {}", &macrify_args.target),
    // };
    //
    // if !has_arg_with_ident(&input, "target") {
    //     panic!("no target arg");
    // }
    //
    // if let Some(_) = &macrify_args.args {
    //     if !has_arg_with_ident(&input, "args") {
    //         panic!("no args arg");
    //     }
    // }
    //
    //
    //
    // // TODO: improve spans?
    // original_fn.sig.ident = syn::Ident::new(
    //     &format!("__original_{}", input_ident),
    //     proc_macro2::Span::call_site(),
    // );
    //
    // let original_ident = original_fn.sig.ident.clone();
    //
    // original_fn.vis = Visibility::Inherited;
    //
    // let call = match &macrify_args.args {
    //     Some(args) => {
    //         let args_ty = syn::Ident::new(&args, proc_macro2::Span::call_site());
    //         quote! {
    //             {
    //                 let args: #args_ty = #args_ty::from_list(&syn::parse_macro_input!(a as syn::AttributeArgs)).expect("failed to parse args");
    //                 #original_ident(&mut target, args);
    //             }
    //         }
    //     }
    //     None => quote! {
    //         {
    //             #original_ident(&mut target);
    //         }
    //     },
    // };
    //

    TokenStream::from(quote::quote! {
        #original_fn

        #[proc_macro_attribute]
        pub fn #input_ident(a: proc_macro::TokenStream, i: proc_macro::TokenStream) -> proc_macro::TokenStream {
            use quote::ToTokens;

            let mut target = syn::parse_macro_input!(i as #target_syn_ty);

            #original_ident(&mut target);

            target.into_token_stream().into()
        }
    })
}

#[proc_macro_attribute]
pub fn macra_derive(args: TokenStream, input: TokenStream) -> TokenStream {
    let input = syn::parse_macro_input!(input as ItemFn);
    let args = syn::parse_macro_input!(args as AttributeArgs);

    for meta in args {
        match meta {
            syn::NestedMeta::Lit(lit) => {
                dbg!(lit.to_token_stream());
            },
            syn::NestedMeta::Meta(meta) => {}
        }
    }

    let input_ident = &input.sig.ident;
    let mut original_fn = input.clone();

    original_fn.sig.ident = syn::Ident::new(
        &format!("__original_{}", input_ident),
        proc_macro2::Span::call_site(),
    );

    let original_ident = &original_fn.sig.ident;

    let target_syn_ty = quote::quote!(syn::ItemStruct);

    TokenStream::from(quote::quote! {
        #original_fn

        #[proc_macro_derive(MyTrait)]
        pub fn #input_ident(item: proc_macro::TokenStream) -> proc_macro::TokenStream {
            use quote::ToTokens;

            let mut target = syn::parse_macro_input!(item as #target_syn_ty);

            let mut outer_scope: syn::ItemMod = syn::parse_quote!(mod __macra_temp_mod {});

            #original_ident(&mut target, &mut outer_scope);

            let mut out = proc_macro2::TokenStream::new();

            outer_scope.content.unwrap().1.into_iter().for_each(|item| item.to_tokens(&mut out));

            out.into()
        }
    })
}