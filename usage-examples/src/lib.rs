use macra::macra;
use macra::macra_derive;
use macra::Struct;
use macra::Mod;

#[macra(target = "struct")]
fn add_field_to_struct(target: &mut impl Struct) {
    target.add_field(syn::parse_quote! {
        pub some_field: usize,
    });
}

// #[macra_derive(target = "struct", derive = "MyTrait")]
// fn derive_trait_for_struct(target: &mut impl Struct, outer_scope: &mut impl Mod) {
//     let ident = target.ident();
//
//     // Find the first unit type field
//     let unit_field = target.named_fields().find(|field| field.ty == syn::parse_quote!{ () }).unwrap();
//     let unit_field_ident = unit_field.ident.as_ref().unwrap();
//
//     // Implement TryInto<()> using that field
//     outer_scope.add_item(syn::parse_quote! {
//         impl ::std::convert::TryInto<()> for #ident {
//             type Error = ();
//
//             fn try_into(self) -> Result<(), Self::Error> {
//                 Ok(self.#unit_field_ident)
//             }
//         }
//     });
// }

#[macra_derive(target = "struct", derive = "HeapSize")]
fn derive_heap_size(target: &mut impl Struct, outer_scope: &mut impl Mod) {
    let ident = target.ident();

    let generics =
}