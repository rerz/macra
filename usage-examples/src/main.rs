use usage_examples::add_field_to_struct;
use usage_examples::MyTrait;
use std::convert::TryInto;

#[add_field_to_struct]
#[derive(Default)]
pub struct MyStruct {

}

#[derive(MyTrait)]
pub struct DerivedStruct {
    field: (),
}

fn main() {
    let s = DerivedStruct { field: () };

    let _: () = s.try_into().unwrap();
}