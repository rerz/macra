pub use macra_macros::*;
use syn::punctuated::Punctuated;
use syn::{Field, Variant, FieldsNamed, Item, ItemStruct, ItemEnum, ItemMod};
use syn::__private::quote::__private::Ident;

pub trait Identifiable {
    fn ident(&self) -> syn::Ident;
}

// https://github.com/dtolnay/syn/issues/651#issuecomment-503771863
// Required because `syn::Field` does not implement `syn::Parse` by default
pub struct ParsableNamedField {
    pub field: syn::Field,
}

impl syn::parse::Parse for ParsableNamedField {
    fn parse(input: syn::parse::ParseStream<'_>) -> syn::parse::Result<Self> {
        let field = syn::Field::parse_named(input)?;

        Ok(ParsableNamedField { field })
    }
}


pub trait Struct: Identifiable {
    fn add_field(&mut self, punctuated_fields: Punctuated<ParsableNamedField, syn::Token![,]>);

    fn named_fields(&self) -> Box<dyn Iterator<Item = &Field> + '_>;
}

impl Identifiable for ItemStruct {
    fn ident(&self) -> Ident {
        self.ident.clone()
    }
}

impl Struct for syn::ItemStruct {
    fn add_field(&mut self, punctuated_fields: Punctuated<ParsableNamedField, syn::Token![,]>) {
        match &mut self.fields {
            syn::Fields::Named(named) => {
                for parsed_field in punctuated_fields {
                    named.named.push(parsed_field.field);
                }
            },
            _ => panic!("Only named fields are supported right now"),
        }
    }

    fn named_fields(&self) -> Box<dyn Iterator<Item=&Field> + '_> {
        match &self.fields {
            syn::Fields::Named(named) => Box::new(named.named.iter()),
            _ => panic!()
        }
    }
}

pub trait Enum: Identifiable {
    fn add_variant(&mut self, variant: Punctuated<Variant, syn::Token![,]>);
}

impl Identifiable for ItemEnum {
    fn ident(&self) -> Ident {
        self.ident.clone()
    }
}

impl Enum for syn::ItemEnum {
    fn add_variant(&mut self, punctuated_variants: Punctuated<Variant, syn::Token![,]>) {
        for variant in punctuated_variants {
            self.variants.push(variant);
        }
    }
}

pub trait Mod: Identifiable {
    fn add_item(&mut self, item: syn::Item);
}

impl Identifiable for ItemMod {
    fn ident(&self) -> Ident {
        self.ident.clone()
    }
}

impl Mod for syn::ItemMod {
    fn add_item(&mut self, item: Item) {
        let (_, items) = self.content.as_mut().unwrap();
        items.push(item);
    }
}